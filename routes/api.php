<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
Route::group(['middleware' => 'auth:api'], function(){
    //return $request->user();
    Route::get('get_details', 'API\UserController@getDetails');
    Route::post('create_sessions', 'API\UserController@create_sessions');
    Route::get('session_share_users_list', 'API\UserController@session_share_users_list');
    Route::any('send_request_to_users', 'API\UserController@send_request_to_users');
    Route::post('users_session_request_list', 'API\UserController@users_session_request_list');
    Route::post('user_accept_reject_session', 'API\UserController@user_accept_reject_session');
    Route::post('user_complete_session', 'API\UserController@user_complete_session');
    Route::post('publisher_end_session', 'API\UserController@publisher_end_session');
    Route::get('active_sessions_list', 'API\UserController@active_sessions_list');
    Route::post('search_users_list', 'API\UserController@search_users_list');
    Route::any('session_users_list', 'API\UserController@session_users_list');
});
Route::post('register', 'API\UserController@register');
Route::post('login', 'API\UserController@login');
Route::post('forget_user', 'API\UserController@forget_user');
Route::put('update_password', 'API\UserController@update_password');
Route::any('test_mail', 'API\UserController@test_mail');
Route::post('mobile_verified', 'API\UserController@mobile_verified');
Route::post('resend_otp_phone', 'API\UserController@resend_otp_phone');
Route::post('resend_otp_email', 'API\UserController@resend_otp_email');
Route::post('email_verified', 'API\UserController@email_verified');
Route::any('otp_expire', 'API\UserController@otp_expire');
