<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imtr_participants', function (Blueprint $table) {
             //$table->id();
             $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('session_id')->unsigned();
            $table->index('session_id');
            $table->foreign('session_id')->references('id')->on('imtr_sessions')->onDelete('cascade');
            $table->integer('type')->default(2)->comment('1=>publisher , 2=>subscriber')->nullable();
            $table->date('participate_date')->nullable();
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->datetime('session_end_time')->nullable();
            $table->integer('status')->default(false)->comment('0=>invitation set , 1=>accept,2=>reject,3=>disconnected,4=>paused,5=>completed,6=>end session');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imtr_participants');
    }
}
