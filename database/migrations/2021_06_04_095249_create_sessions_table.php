<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imtr_sessions', function (Blueprint $table) {
            //$table->id();
             $table->unsignedBigInteger('id')->primary();
             $table->unsignedBigInteger('user_id')->unsigned();
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('title')->nullable();
            $table->integer('duration')->nullable();
            $table->string('duration_type')->nullable();
            $table->integer('charge_for_min')->default(false);
            $table->text('description')->nullable();
            $table->integer('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imtr_sessions');
    }
}
