import requests
import json
import sys
from bs4 import BeautifulSoup
#url ='https://egov.uscis.gov/casestatus/mycasestatus.do?appReceiptNum=WAC1690351000'
url = 'https://egov.uscis.gov/casestatus/mycasestatus.do?appReceiptNum='+sys.argv[1]
page = requests.get(url)
soup = BeautifulSoup(page.content, "html.parser")
job_elements = soup.find_all("div", class_="rows text-center")


res = []
status = job_elements[0].find_all("h1")
#res.append(status[0].text)
#print( "Status: " +  status[0].text)
moreinfor = job_elements[0].find_all("p")
#res.append(moreinfor[0].text)

value = {
    "status": status[0].text,
    "more_info": moreinfor[0].text,
    }
print (json.dumps(value))
#print("More INFO: " +  status[0].text)
