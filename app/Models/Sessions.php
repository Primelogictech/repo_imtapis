<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    protected $table = "imtr_sessions";
    protected $fillable = ['user_id', 'title', 'duration', 'duration_type', 'charge_for_min', 'description', 'status', 'created_at', 'updated_at'
    ];
   
}