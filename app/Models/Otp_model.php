<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp_model extends Model
{
    protected $table = "otp_verification";
    protected $fillable = ['user', 'otp', 'status', 'created_at', 'updated_at'
    ];
   
}