<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    protected $table = "imtr_participants";
    protected $fillable = ['user_id', 'session_id', 'type', 'participate_date', 'start_time', 'end_time','session_end_time', 'status', 'created_at', 'updated_at'
    ];

    public function session_details()
    {
        return $this->hasOne('App\Models\Sessions','id','session_id');
    }
    public function user_details()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }
   
}