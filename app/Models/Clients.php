<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = 'client';
    public $incrementing = false;
    use HasFactory;
    protected $fillable = ['id', 'client_id', 'topic', 'status', 'created_at', 'updated_at'];
}
