<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use PhpMqtt\Client\ConnectionSettings;
use PhpMqtt\Client\Examples\Shared\SimpleLogger;
use PhpMqtt\Client\Exceptions\MqttClientException;
use PhpMqtt\Client\MqttClient;
use Psr\Log\LogLevel;

class Publisher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mqtt:publish {message} {client}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            // Create a new instance of an MQTT client and configure it to use the shared broker host and port.
            $client = new MqttClient('localhost', 1883, $this->argument('client'), MqttClient::MQTT_3_1, null);

            // Create and configure the connection settings as required. The provided last will is the message the broker will publish
            // on behalf of the client when the client disconnects abnormally (i.e. no graceful disconnect).
            $connectionSettings = (new ConnectionSettings)
                ->setLastWillTopic('test/client/disconnected')
                ->setLastWillMessage('disconnected:' . $this->argument('client'))
                ->setLastWillQualityOfService(MqttClient::QOS_AT_LEAST_ONCE)
                ->setRetainLastWill(true);

            // Connect to the broker with the configured connection settings and with a clean session.
            $client->connect($connectionSettings, true);

            // Publish the message 'online' on the topic 'test/client/test-publisher' using QoS 1.
            $client->publish('test/client/connect', 'connected:' . $this->argument('client'), MqttClient::QOS_AT_LEAST_ONCE);
            // return 'Connected';

            while (true) {
            }

            // Do not terminate the connection to the broker gracefully, to trigger publishing of our last will.
            // $client->disconnect();
        } catch (MqttClientException $e) {
            return $e;
            // MqttClientException is the base exception of all exceptions in the library. Catching it will catch all MQTT related exceptions.
            // $logger->error('Connecting with last will or publishing online state with QoS 1 failed. An exception occurred.', ['exception' => $e]);
        }

        // return 'Executed';

    }
}
