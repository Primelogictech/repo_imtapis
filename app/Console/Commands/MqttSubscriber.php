<?php

namespace App\Console\Commands;

use App\Models\Clients;
use Illuminate\Console\Command;
use PhpMqtt\Client\Facades\MQTT;

class MqttSubscriber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mqtt:subscribe {topic}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $mqtt = new Mqtt();
        // $mqtt->ConnectAndSubscribe('RaviVarma', function ($topic, $msg) {
        //     echo "Msg Received: \n";
        //     // echo "Topic: {$topic}\n\n";
        //     echo "\t$msg\n\n";
        // }, 123456);


        // $server   = 'localhost';
        // $port     = 1883;
        // $clientId = 'test-publisher';

        // $mqtt = new \PhpMqtt\Client\MqttClient($server, $port, $clientId);
        // $mqtt->connect();
        // $mqtt->subscribe('php-mqtt/client/test', function ($topic, $message) {
        //     echo sprintf("Received message on topic [%s]: %s\n", $topic, $message);
        // }, 0);
        // $mqtt->loop(true);
        // $mqtt->disconnect();

        $mqtt = MQTT::connection();
      
        $mqtt->subscribe($this->argument('topic'), function (string $topic, string $message) {
            echo sprintf('[%s]: %s', $topic, substr($message, 12));
            $c =Clients::all();
            if ($topic == 'test/client/disconnected') {
                echo '%%%%%%%%%%%%%%%%%%%%%%%';
                $client = Clients::firstOrNew(['client_id' => substr($message, 13), 'topic' => 'test']);
                if(!$client->id){
                    $client->id = count($c) + 1;
                }
                $client->status = 'offline';
                $client->save();
                echo $message;
            } else if ($topic == 'test/client/connect') {
                echo '####################';
                $client = Clients::firstOrNew(['client_id' => substr($message, 10), 'topic' => 'test']);
                $client->status = 'online';
                if(!$client->id){
                    $client->id = count($c) + 1;
                }
                $client->save();
                echo $message;
            }
            // echo $message . ' ------- ';
        }, 1);
        $mqtt->loop(true);
    }
}
