<?php

namespace App\Http\Controllers\API;
require  base_path().'/vendor/autoload.php';
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Otp_model;
use App\Models\Sessions;
use App\Models\Participants;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Collection;

use Mail;


class UserController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'phonenumber' => 'required|unique:users',
            'phone_code'=>'required',
          
        ],[
    'required' => 'The :attribute Required.',
    'unique' => 'The :attribute already exist.'
   
]);
        if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
       
          $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        
          $user = User::create($input);

          $mobile_otp = rand(100000,999999);

          Otp_model::updateOrCreate(['user'=>$request->phonenumber],['otp'=>$mobile_otp]);
          $email_otp = rand(100000,999999);

          Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$email_otp]);

          $account_sid = 'ACa82f67d459e04d67af19095fb6b7bc3a';
          $auth_token = '63bee279ffa078ea8ee3552b816cddf0';
          $twilio_number = "+19292948885";
          $client = new Client($account_sid, $auth_token);
          $client->messages->create(
             $request->phone_code.$request->phonenumber,
              array(
                  'from' => $twilio_number,
                  'body' =>"Hi, OTP is ".$mobile_otp
              )
          );
          $msg= "Hi, OTP is ".$email_otp;
          Mail::raw($msg, function ($message) use ($request) {
            $message->to($request->email)
              ->subject('OTP verification to IMTR');
          });

           if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
             $success['token'] =  $user->createToken('MyApp')->accessToken;
             return response()->json(['success'=>"Registration Successfully Completed",'token' => $success['token'],'Details'=>$user,'error'=>false], $this->successStatus);
           }else{

       
            return response()->json(['success'=>"Registration Successfully Completed",'Details'=>$user,'error'=>false,'code'=>200], $this->successStatus);
          }

    }
    public function login(Request $request){
      
      if(!empty(request()->input('username'))&&!empty(request()->input('password'))){

      $login = request()->input('username');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phonenumber';

        if(Auth::attempt([$fieldType => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            if($user->phone_verified==0){
              $mobile_otp = rand(100000,999999);

          Otp_model::updateOrCreate(['user'=>$user->phonenumber],['otp'=>$mobile_otp]);
        
              $account_sid = 'ACa82f67d459e04d67af19095fb6b7bc3a';
              $auth_token = '63bee279ffa078ea8ee3552b816cddf0';
              $twilio_number = "+19292948885";
              $client = new Client($account_sid, $auth_token);
              $client->messages->create(
                 $user->phone_code.$user->phonenumber,
                  array(
                      'from' => $twilio_number,
                      'body' =>"Hi, OTP is ".$mobile_otp
                  )
              );
            }
            if($user->email_verified==0){
               $email_otp = rand(100000,999999);

            Otp_model::updateOrCreate(['user'=>$user->email],['otp'=>$email_otp,'status'=>1]);
            $msg= "Hi, OTP is ".$email_otp;
            Mail::raw($msg, function ($message) use ($user) {
              $message->to($user->email)
                ->subject('OTP verification to IMTR');
            });
        }
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success,'Details'=>$user,'error'=>false], $this->successStatus);
        }
        else{
            return response()->json(['error'=>true,'error_msg'=>"Login Details Wrong"], 200);
        }
      }else{
        return response()->json(['error'=>true,'error_msg'=>"All Fileds Required"], 200);
      }
    }
    public function getDetails()
    {
        $user = Auth::user();
        return response()->json(['success' => $user,'error'=>false], $this->successStatus);
    }
    public function test_mail($value='')
    {
      $msg= "Hi, OTP is 1234567890";
          Mail::raw($msg, function ($message)  {
            $message->to('madhukar8500@gmail.com')
              ->subject('OTP verification to IMTR');
          });
          dd('success');
    }
    public function otp_expire($value='')
    {
      
      Otp_model::where('created_at','<=',date('Y-m-d H:i:s', strtotime('-15 minutes')))->update(['status'=>0]);
    }
    public function mobile_verified(Request $request)
    {
      if(isset($request->mobile_number)&& isset($request->otp)){
      
        $otp_verify = Otp_model::where(['user'=>$request->mobile_number,'otp'=>$request->otp,'status'=>1])->first();
          if(!empty($otp_verify)){
            $otp_verify->status=0;
            $otp_verify->save();
            User::where('phonenumber',$request->mobile_number)->update(['phone_verified'=>1]);
            return response()->json(['msg'=>'Phone number Verified Successfully Completed','error'=>false], 200);
          }else{
             return response()->json(['error_msg'=>'Otp Invalid','error'=>true], 200);
          }
        }else{
           return response()->json(['error_msg'=>'All fields required','error'=>true], 200);
      }
    }
    public function resend_otp_phone(Request $request)
    {
      if(isset($request->mobile_number)&& isset($request->phone_code)){

         $user = User::where('phonenumber',$request->mobile_number)->first();
         if(!empty($user)){

          if(!$user->phone_verified){
      
        $mobile_otp = rand(100000,999999);

          Otp_model::updateOrCreate(['user'=>$request->mobile_number],['otp'=>$mobile_otp,'status'=>1]);
          $account_sid = 'ACa82f67d459e04d67af19095fb6b7bc3a';
          $auth_token = '63bee279ffa078ea8ee3552b816cddf0';
          $twilio_number = "+19292948885";
          $client = new Client($account_sid, $auth_token);
          $client->messages->create(
             $request->phone_code.$request->mobile_number,
              array(
                  'from' => $twilio_number,
                  'body' =>"Hi, OTP is ".$mobile_otp
              )
          );
          
            return response()->json(['msg'=>'OTP Sent Successfully','error'=>false], 200);
         
             }else{
                 return response()->json(['error_msg'=>'Phone Number Already Verified','error'=>true], 200);
            }
          }else{
             return response()->json(['error_msg'=>'Phone Number Not Registered','error'=>true], 200);
        }
      }else{
           return response()->json(['error_msg'=>'All fields required','error'=>true], 200);
      }
    }
    public function resend_otp_email(Request $request)
    {
      if(isset($request->email)){
        $user = User::where('email',$request->email)->first();
         if(!empty($user)){

          if(!$user->email_verified){
      
        $email_otp = rand(100000,999999);

          Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$email_otp,'status'=>1]);
          $msg= "Hi, OTP is ".$email_otp;
          Mail::raw($msg, function ($message) use ($request) {
            $message->to($request->email)
              ->subject('OTP verification to IMTR');
          });
          
            return response()->json(['msg'=>'OTP Sent Successfully','error'=>false], 200);
         }else{
                 return response()->json(['error_msg'=>'Email Already Verified','error'=>true], 200);
            }
          }else{
             return response()->json(['error_msg'=>'Email Not Registered','error'=>true], 200);
        }
        }else{
           return response()->json(['error_msg'=>'All fields required','error'=>true], 200);
      }
    }
    public function email_verified(Request $request)
    {
      if(isset($request->email)&& isset($request->otp)){
      
        $otp_verify = Otp_model::where(['user'=>$request->email,'otp'=>$request->otp,'status'=>1])->first();
          if(!empty($otp_verify)){
            $otp_verify->status=0;
            $otp_verify->save();
            User::where('email',$request->email)->update(['email_verified'=>1]);
            return response()->json(['msg'=>'Email Verified Successfully Completed','error'=>false], 200);
          }else{
             return response()->json(['error_msg'=>'Otp Invalid','error'=>true], 200);
          }
        }else{
           return response()->json(['error_msg'=>'All fields required','error'=>true], 200);
      }
    }

    public function forget_user(Request $request)
    {
      if(isset($request->username)){
         $user = User::where('email',$request->username)->orWhere('phonenumber',$request->username)->first();
         if(!empty($user)){

          $otp_number = rand(100000,999999);

            Otp_model::updateOrCreate(['user'=>$request->username],['otp'=>$otp_number]);

            $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phonenumber';
            if($fieldType=="email"){
          $messages = 'Hi, Your Forget OTP Number '.$otp_number;

          Mail::raw($messages, function ($message) use ($request) {
              $message->to($request->username)
                ->subject("Forget Mail");
            });
            }else{
               $account_sid = 'ACa82f67d459e04d67af19095fb6b7bc3a';
          $auth_token = '63bee279ffa078ea8ee3552b816cddf0';
          $twilio_number = "+19292948885";
          $client = new Client($account_sid, $auth_token);
          $client->messages->create(
             $user->phone_code.$request->username,
              array(
                  'from' => $twilio_number,
                  'body' =>"Hi, Your Forget OTP Number ".$otp_number
              )
          );
            }
          return response()->json(['msg'=>'OTP Send Successfully','error'=>false], 200);

         }else{
        return response()->json(['error_msg'=>'User Not Exist','error'=>true], 200);
        }
      }else{
        return response()->json(['error_msg'=>'All fields required','error'=>true], 200);
      }
    }
    public function update_password(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|min:8',
            'otp'=>'required',
          
        ],[
    'required' => 'The :attribute Required.',
    'unique' => 'The :attribute already exist.'
   
]);
        if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
      if(isset($request->username)&&isset($request->otp)&&isset($request->password)){
          $user = User::where('email',$request->username)->orWhere('phonenumber',$request->username)->first();
         if(!empty($user)){
           $users = Otp_model::where(['user'=>$request->username,'otp'=>$request->otp])->first();
           if(!empty($users)){
            User::where('email',$request->username)->orWhere('phonenumber',$request->username)->update(['password'=>bcrypt($request->password)]);
            return response()->json(['msg'=>'Password Updated Successfully','error'=>false], 200);
           }else{
             return response()->json(['error_msg'=>'Otp InValid','error'=>true], 200);
           }
          
          }else{
        return response()->json(['error_msg'=>'User Not Exist','error'=>true], 200);
        }
      }else{
        return response()->json(['error_msg'=>'All fields required','error'=>true], 200);
      }
    }

    public function create_sessions(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'title' => 'required',
            'duration' => 'required',
            'duration_type'=>'required',
            'charge_for_min' => 'required',
            'description' => 'required',
                      
        ],[
      'required' => 'The :attribute Required.'
      ]);
      if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }

          $check = Sessions::where(['user_id'=>Auth::user()->id,'status'=>1])->first();

          if(empty($check)){

      $sessions = Sessions::updateOrCreate(['id'=>$request['id']],['title'=>$request['title'],'duration'=>$request['duration'],'duration_type'=>$request['duration_type'],'charge_for_min'=>$request['charge_for_min'],'description'=>$request['description'],'user_id'=>Auth::user()->id]);
      if(!empty($sessions)){

        Participants::create(['user_id'=>Auth::user()->id, 'session_id'=>$sessions->id, 'type'=>1, 'participate_date'=>date('Y-m-d'), 'start_time'=>date('Y-m-d H:i:s'), 'end_time'=>null, 'status'=>1]);

        return response()->json(['msg'=>'Session Created Successfully','error'=>false,'details'=>$sessions], 200);

      }else{
        return response()->json(['error_msg'=>'Something Went Wrong','error'=>true], 200);
      }
    }else{
      return response()->json(['error_msg'=>'Sessions Already Created','error'=>true], 200);
    }
      
    }
    public function active_sessions_list($value='')
    {
      $sessions = Sessions::where('user_id',Auth::user()->id)->where(['status'=>1])->get()->toArray();
      if(!empty($sessions)){
        return response()->json(['msg'=>'Sessions List','details'=>$sessions,'error'=>false], 200);

      }else{
        return response()->json(['error_msg'=>'Active Sessions Not Found','error'=>true], 200);
      }
    }
    public function search_users_list(Request $request)
    {
       $login = request()->input('username');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phonenumber';
          $validator = Validator::make($request->all(), [
            'username' => 'required',             
        ],[
      'required' => 'The :attribute Required.'
      ]);

      if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }

      $users = User::where('id','!=',Auth::user()->id)->where(['email_verified'=>1,'phone_verified'=>1,$fieldType => request('username')])->get()->toArray();
      if(!empty($users)){
        return response()->json(['msg'=>'Users List','details'=>$users,'error'=>false], 200);

      }else{
        return response()->json(['error_msg'=>'Users Not Found','error'=>true], 200);
      }
    }

    public function session_share_users_list($value='')
    {
      $users = User::where('id','!=',Auth::user()->id)->where(['email_verified'=>1,'phone_verified'=>1])->get()->toArray();
      if(!empty($users)){
        return response()->json(['msg'=>'Users List','details'=>$users,'error'=>false], 200);

      }else{
        return response()->json(['error_msg'=>'Users Not Found','error'=>true], 200);
      }
    }
    public function send_request_to_users(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'session_id' => 'required|exists:imtr_sessions,id',
                   
        ],[
      'required' => 'The :attribute Required.',
      'exists' => 'The :attribute is Invalid.'
      ]);
      if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
          try{
            $check = Participants::where(['user_id'=>$request->user_id, 'session_id'=>$request->session_id])->first();
            if(empty($check)){
         $participant= Participants::create(['user_id'=>$request->user_id, 'session_id'=>$request->session_id, 'type'=>2, 'participate_date'=>date('Y-m-d'), 'start_time'=>null, 'end_time'=>null, 'status'=>0]);

         if(!empty($participant)){
           return response()->json(['msg'=>'Request Sent Successfully','error'=>false], 200);

         }else{
           return response()->json(['error_msg'=>'Something Went Wrong','error'=>true], 200);
         }
        }else{
           return response()->json(['error_msg'=>'Already Sent Request','error'=>true], 200);
        }
       }catch(Exception $e){
        return response()->json(['error_msg'=>$e->message(),'error'=>true], 200);
       }
    }
    public function users_session_request_list(Request $required)
    {
      $list = Participants::with(['session_details'])->where(['user_id'=>Auth::user()->id,'status'=>0])->get()->toArray();
       if(!empty($list)){
        return response()->json(['msg'=>'Sessions List','details'=>$list,'error'=>false], 200);

      }else{
        return response()->json(['error_msg'=>'Sessions Not Found','error'=>true], 200);
      }

    }

    public function user_accept_reject_session(Request $request)
    {
      $validator = Validator::make($request->all(), [


           'session_id' => 'required|exists:imtr_sessions,id',
           'request_id' => 'required|exists:imtr_participants,id',
           'status' => 'required|integer|between:1,2',
                   
        ],[
      'required' => 'The :attribute Required.',
      'exists' => 'The :attribute is Invalid.',
      'integer' => 'The :attribute must be integer.',
      'between' => 'The :attribute must be between 1 to 2.'
      ]);
      if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
          try{
          $session = Sessions::where(['id'=>$request->session_id,'status'=>1])->first();
            if(!empty($session)){

              if($request->status==1){

              $check = Participants::where(['user_id'=>Auth::user()->id,'status'=>1])->first();
              if(empty($check)){

                Participants::where(['user_id'=>Auth::user()->id,'id'=>$request->request_id])->update(['status'=>1,'start_time'=>date('Y-m-d H:i:s')]);

                return response()->json(['msg'=>'Your Sessions Started Successfully','error'=>false], 200);
                }else{
                return response()->json(['error_msg'=>'Your Already Participant Another Sessions','error'=>true], 200);
              }
              }else{
                 Participants::where(['user_id'=>Auth::user()->id,'id'=>$request->request_id])->update(['status'=>$request->status]);

              return response()->json(['msg'=>'Your Sessions Rejected Successfully','error'=>false], 200);
              }

            }else{
              return response()->json(['error_msg'=>'Sessions Already Expired','error'=>true], 200);
            }

          }catch(Exception $e){
        return response()->json(['error_msg'=>$e->message(),'error'=>true], 200);
       }
    }
    public function user_complete_session(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'session_id' => 'required|exists:imtr_sessions,id',
                 
        ],[
      'required' => 'The :attribute Required.'
      ]);
      if ($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
           $sesseion_check = Sessions::where(['user_id'=>Auth::user()->id,'id'=>$request->session_id,'status'=>1])->first();
        if(!empty($sesseion_check)){

           $update = Participants::where(['user_id'=>Auth::user()->id,'session_id'=>$request->session_id])->update(['status'=>5,'end_time'=>date('Y-m-d H:i:s')]);
           if($update){
            return response()->json(['error_msg'=>'Sessions End Successfully','error'=>false], 200);

           }else{
            return response()->json(['error_msg'=>'Something Went Wrong','error'=>true], 200);
           }
           }else{
          return response()->json(['error_msg'=>'Your Session Already Ended','error'=>true], 200);
        }
    }
    public function publisher_end_session(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'session_id' => 'required|exists:imtr_sessions,id',
                 
        ],[
      'required' => 'The :attribute Required.'
      ]);
      if($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
          $sesseion_check = Sessions::where(['user_id'=>Auth::user()->id,'id'=>$request->session_id,'status'=>1])->first();
        if(!empty($sesseion_check)){
           $session = Sessions::where(['user_id'=>Auth::user()->id,'id'=>$request->session_id])->update(['status'=>0]);

          Participants::where('session_id',$request->session_id)->update(['session_end_time'=>date('Y-m-d H:i:s')]);
          Participants::where('session_id',$request->session_id)->where('status','!=',5)->where('status','!=',2)->update(['status'=>6]);
          return response()->json(['error_msg'=>'Sessions End Successfully','error'=>false], 200);

        }else{
          return response()->json(['error_msg'=>'Session Already Expired','error'=>true], 200);
        }
    }
    public function Session_users_list(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'session_id' => 'required|exists:imtr_sessions,id',
                 
        ],[
      'required' => 'The :attribute Required.'
      ]);
      if($validator->fails()) {
            return response()->json(['error_msg'=>$validator->errors()->first(),'error'=>true,'code'=>401], 200);
          }
          $sesseion_check = Sessions::where(['id'=>$request->session_id,'status'=>1])->first();
        if(!empty($sesseion_check)){
         
          $list = Participants::where('session_id',$request->session_id)->with(['user_details'])->where('user_id','!=',Auth::user()->id)->get();
         
           return response()->json(['msg'=>'Session Users List','details'=>$list,'error'=>false,'status_decription'=>'0=>invitation set , 1=>accept, 2=>reject, 3=>disconnect, 4=>paused, 5=>completed, 6=>publisher end session'], 200);

        }else{
          return response()->json(['error_msg'=>'Session Already Expired','error'=>true], 200);
        }
    }

}
