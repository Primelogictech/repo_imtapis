<?php

namespace App\Exceptions;

use Exception;

class ApiMethodException extends Exception
{
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
    	//dd($request);
         return response()->json( [
                                        'error' => true,
                                        'message' => 'Method is not allowed for the requested route',
                                    ], 405 );
    }
}
