import { Strategy } from "passport-local";
import { User } from "src/entity/user.entity";
import { Repository } from "typeorm";
declare const LocalStrategy_base: new (...args: any[]) => Strategy;
export declare class LocalStrategy extends LocalStrategy_base {
    private readonly UserRepository;
    private readonly logger;
    constructor(UserRepository: Repository<User>);
    validate(username: string, password: string): Promise<any>;
}
export {};
