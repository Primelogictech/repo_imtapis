import { JwtService } from "@nestjs/jwt";
import { User } from "src/entity/user.entity";
export declare class AuthService {
    private readonly jwtService;
    constructor(jwtService: JwtService);
    getjwttoken(user: User): string;
    haspassword(password: string): Promise<string>;
}
