"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const auth_service_1 = require("../auth/auth.service");
const user_entity_1 = require("../entity/user.entity");
const typeorm_2 = require("typeorm");
const user_dto_1 = require("./user.dto");
const user_service_1 = require("./user.service");
let UserController = class UserController {
    constructor(UserRepository, userService, authService) {
        this.UserRepository = UserRepository;
        this.userService = userService;
        this.authService = authService;
    }
    async createUser(userDto) {
        const user = new user_entity_1.User();
        if (userDto.password !== userDto.retypepassword) {
            throw new common_1.BadRequestException(['Password not equel']);
        }
        const existUser = await this.UserRepository.findOne({
            where: [{ email: userDto.email }, { phone: userDto.phone }, { username: userDto.username }]
        });
        if (existUser) {
            throw new common_1.BadRequestException(['email or phone or username already exist']);
        }
        user.name = userDto.name;
        user.email = userDto.email;
        user.phone = userDto.phone;
        user.password = await this.authService.haspassword(userDto.password);
        user.address = userDto.address;
        user.status = true;
        user.created_at = new Date().toISOString();
        user.username = userDto.username;
        return Object.assign(Object.assign({}, (await this.UserRepository.save(user))), { token: this.authService.getjwttoken(user) });
    }
    getHello() {
        return this.userService.getHello();
    }
    getAllUsersData() {
        return this.userService.getAllUser();
    }
};
__decorate([
    common_1.Post('register'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_dto_1.UserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "createUser", null);
__decorate([
    common_1.Get('/'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], UserController.prototype, "getHello", null);
__decorate([
    common_1.Get('/userslist'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getAllUsersData", null);
UserController = __decorate([
    common_1.Controller('user'),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        user_service_1.UserService, auth_service_1.AuthService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map