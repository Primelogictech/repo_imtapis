import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/entity/user.entity';
import { Repository } from 'typeorm';
import { UserDto } from './user.dto';
import { UserService } from './user.service';
export declare class UserController {
    private readonly UserRepository;
    private readonly userService;
    private readonly authService;
    constructor(UserRepository: Repository<User>, userService: UserService, authService: AuthService);
    createUser(userDto: UserDto): Promise<{
        token: string;
        id: number;
        name: string;
        username: string;
        email: string;
        password: string;
        phone: string;
        status: boolean;
        address: string;
        created_at: string;
    }>;
    getHello(): string;
    getAllUsersData(): Promise<User[]>;
}
