export declare class UserDto {
    name: string;
    email: string;
    phone: string;
    password: string;
    retypepassword: string;
    address: string;
    status: boolean;
    created_at: string;
    username: string;
}
