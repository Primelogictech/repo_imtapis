import { User } from 'src/entity/user.entity';
import { Repository } from 'typeorm';
export declare class UserService {
    private UserRepository;
    constructor(UserRepository: Repository<User>);
    getAllUser(): Promise<User[]>;
    getOneUserById(id: number): Promise<User>;
    CreateUser(data: any): Promise<User>;
    UpdateUser(id: number, email: string): Promise<User>;
    DeleteUser(id: number): Promise<User>;
    CustomQuery(): any;
    getHello(): string;
}
