"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../entity/user.entity");
const typeorm_2 = require("typeorm");
let UserService = class UserService {
    constructor(UserRepository) {
        this.UserRepository = UserRepository;
    }
    getAllUser() {
        return this.UserRepository.find();
    }
    async getOneUserById(id) {
        try {
            const user = await this.UserRepository.findOneOrFail(id);
            return user;
        }
        catch (err) {
            throw err;
        }
    }
    async CreateUser(data) {
        const user = await this.UserRepository.create({
            name: data.name,
            email: data.email,
            password: data.password,
            phone: data.phone,
            status: data.status,
            address: data.address,
            created_at: data.created_at
        });
        return this.UserRepository.save(user);
    }
    async UpdateUser(id, email) {
        const user = await this.getOneUserById(id);
        user.email = email;
        return this.UserRepository.save(user);
    }
    async DeleteUser(id) {
        const user = await this.getOneUserById(id);
        this.UserRepository.remove(user);
        return user;
    }
    CustomQuery() {
        return this.UserRepository.createQueryBuilder('user').select('id , name , email ,phone');
    }
    getHello() {
        return JSON.stringify({ 'name': 'Madhukar B!' });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map