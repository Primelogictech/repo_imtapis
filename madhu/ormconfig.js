"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'imtruser',
    password: 'IMTR@App@123!',
    database: 'nodejs',
    entities: [
        'entity/**/*.entity{.ts,.js}',
    ],
    synchronize: true,
};
exports.default = config;
//# sourceMappingURL=ormconfig.js.map