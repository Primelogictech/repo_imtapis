import { Repository } from 'typeorm';
import { User } from './entity/user.entity';
export declare class AppService {
    private UserRepository;
    constructor(UserRepository: Repository<User>);
    getHello(): string;
}
