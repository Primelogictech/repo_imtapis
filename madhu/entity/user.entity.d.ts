export declare class User {
    id: number;
    name: string;
    username: string;
    email: string;
    password: string;
    phone: string;
    status: boolean;
    address: string;
    created_at: string;
}
