import { Events } from "./events.entity";
export declare class EventUsers {
    id: number;
    user_id: number;
    event_id: number;
    created_at: string;
    events: Events;
}
