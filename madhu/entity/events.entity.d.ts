import { EventUsers } from "./eventusers.entity";
export declare class Events {
    id: number;
    event_name: String;
    event_description: String;
    event_date: String;
    created_at: String;
    updated_at: String;
    event_users: EventUsers[];
}
