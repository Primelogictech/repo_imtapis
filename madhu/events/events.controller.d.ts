import { Events } from 'src/entity/events.entity';
import { Repository } from 'typeorm';
import { EventsDto } from './events.dto';
import { EventsService } from './events.service';
export declare class EventsController {
    private readonly eventsRepository;
    private readonly eventService;
    constructor(eventsRepository: Repository<Events>, eventService: EventsService);
    events(): Promise<Events[]>;
    getSingleevent(id: number): Promise<Events>;
    create_events(eventDto: EventsDto): Promise<object & Events>;
    deleteEvent(id: number): Promise<Events>;
    updateEvent(id: number, input: any): Promise<Events>;
}
