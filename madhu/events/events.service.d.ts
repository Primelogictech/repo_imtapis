import { Events } from 'src/entity/events.entity';
import { Repository } from 'typeorm';
export declare class EventsService {
    private readonly eventRepository;
    constructor(eventRepository: Repository<Events>);
    getAllEvents(): Promise<Events[]>;
    insertEvent(data: object): Promise<object & Events>;
    getEvent(id: number): Promise<Events>;
    deleteEvents(id: number): Promise<Events>;
    updateEvents(id: number, date: String): Promise<Events>;
}
