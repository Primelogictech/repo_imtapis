export declare class EventsDto {
    event_name: String;
    event_description: String;
    event_date: String;
    created_at: String;
}
