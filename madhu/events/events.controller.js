"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsController = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const events_entity_1 = require("../entity/events.entity");
const typeorm_2 = require("typeorm");
const events_dto_1 = require("./events.dto");
const events_service_1 = require("./events.service");
let EventsController = class EventsController {
    constructor(eventsRepository, eventService) {
        this.eventsRepository = eventsRepository;
        this.eventService = eventService;
    }
    events() {
        return this.eventService.getAllEvents();
    }
    getSingleevent(id) {
        console.log('id ' + id);
        return this.eventService.getEvent(id);
    }
    async create_events(eventDto) {
        const events = new events_entity_1.Events();
        const event_exsist = await this.eventsRepository.findOne({
            where: { event_name: eventDto.event_name,
                event_date: eventDto.event_date }
        });
        if (event_exsist) {
            throw new common_1.BadRequestException(['Event Already exist in same date']);
        }
        events.event_date = eventDto.event_date;
        events.event_name = eventDto.event_name;
        events.event_description = eventDto.event_description;
        events.created_at = new Date().toISOString();
        return this.eventService.insertEvent(events);
    }
    deleteEvent(id) {
        return this.eventService.deleteEvents(id);
    }
    updateEvent(id, input) {
        return this.eventService.updateEvents(id, input.event_date);
    }
};
__decorate([
    common_1.Get('all'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "events", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], EventsController.prototype, "getSingleevent", null);
__decorate([
    common_1.Post('create'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [events_dto_1.EventsDto]),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "create_events", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], EventsController.prototype, "deleteEvent", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", void 0)
], EventsController.prototype, "updateEvent", null);
EventsController = __decorate([
    common_1.Controller('events'),
    __param(0, typeorm_1.InjectRepository(events_entity_1.Events)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        events_service_1.EventsService])
], EventsController);
exports.EventsController = EventsController;
//# sourceMappingURL=events.controller.js.map