"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const events_entity_1 = require("../entity/events.entity");
const typeorm_2 = require("typeorm");
let EventsService = class EventsService {
    constructor(eventRepository) {
        this.eventRepository = eventRepository;
    }
    async getAllEvents() {
        return await this.eventRepository.find({ relations: ["event_users"] });
    }
    insertEvent(data) {
        return this.eventRepository.save(data);
    }
    async getEvent(id) {
        try {
            const event = await this.eventRepository.findOne(id);
            if (event) {
                return event;
            }
            throw new common_1.BadRequestException(['Event id Not Found']);
        }
        catch (err) {
            throw err;
        }
    }
    async deleteEvents(id) {
        const event = await this.getEvent(id);
        this.eventRepository.remove(event);
        return event;
    }
    async updateEvents(id, date) {
        const event = await this.getEvent(id);
        event.event_date = date;
        this.eventRepository.save(event);
        return event;
    }
};
EventsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(events_entity_1.Events)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], EventsService);
exports.EventsService = EventsService;
//# sourceMappingURL=events.service.js.map